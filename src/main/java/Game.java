import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.SCMLReader;
import com.brashmonkey.spriter.java2d.Drawer;
import com.brashmonkey.spriter.java2d.Loader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Game extends JPanel {

    private static final AffineTransform identity = new AffineTransform();
    private final Background bg;
    private final Overlay overlay;
    private final Player player;
    private final Drawer drawer;
    private final int height;
    private List<Integer> keyCodes;

    public Game(JFrame frame) {
        setFocusable(true);
        keyCodes = new ArrayList<>();

        bg = new Background();
        overlay = new Overlay();

        String scml = "";
        try {
            // Read the file
            String[] tmp = {""};
            Files.readAllLines(Paths.get(getClass().getResource("PNG_Parts&Spriter_Animation/Flight+Ships.scml").toURI()))
                    .forEach(line -> tmp[0] += line);
            scml = tmp[0];
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        Data data = new SCMLReader(scml).getData();
        player = new Player(data.getEntity(0));
        Loader loader = new Loader(data);
        loader.load("src/main/resources/PNG_Parts&Spriter_Animation");

        drawer = new Drawer(frame, loader);
        height = frame.getContentPane().getHeight();

        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (!keyCodes.contains(e.getKeyCode()))
                    keyCodes.add(e.getKeyCode());
            }

            @Override
            public void keyReleased(KeyEvent e) {
                keyCodes.removeIf(keycode -> keycode.equals(e.getKeyCode()));
            }
        });

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {

            }
        });
    }

    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        bg.paint(g2d);
        overlay.paint(g2d);

        drawer.graphics = g2d;
        // g.clearRect(0, 0, getWidth(), getHeight());
        if(player.getCurrentKey() != null){
            drawer.draw(player);
            drawer.graphics.setTransform(identity);
            drawer.graphics.scale(1, -1);
            drawer.graphics.translate(0, -height);
            // drawer.drawBones(player);
            // drawer.drawBoxes(player);
        }
    }

    public void update() {
        bg.update();
        player.update(keyCodes);
        overlay.update();
    }

    public static void main(String[] args) {
        // Enable hardware acceleration
        System.setProperty("sun.java2d.opengl", "true");
        JFrame frame = new JFrame();
        Game game = new Game(frame);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        frame.setSize(1300, 900);
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2); //x y
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().add(game);
        frame.setTitle("Space Wars");
        frame.setResizable(false);
        frame.setVisible(true);

        while (true) {
            game.update();
            game.repaint();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
