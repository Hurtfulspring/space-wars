import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

public class Background extends Drawable {

    int x1, x2, x3;

    @SuppressWarnings("ConstantConditions")
    public Background() {
        super(0,0,900, 900);
        x1 = 0;
        x2 = 900;
        x3 = 1800;
        Random random = new Random();
        BufferedImage load = null;
        try {
            load = ImageIO.read(getClass().getResource("Backgrounds/" + (random.nextInt(16) + 1) + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        image = load;
    }

    @Override
    public void paint(Graphics2D g2d) {
        g2d.drawImage(image, x1, 0,900,900,null);
        g2d.drawImage(image, x2, 0,900,900,null);
        g2d.drawImage(image, x3, 0,900,900,null);
    }

    @Override
    public void update() {
        x1 -= 1;
        x2 -= 1;
        x3 -= 1;

        if (x1 <= -900)
            x1 = x3 + 900;
        if (x2 <= -900)
            x2 = x1 + 900;
        if (x3 <= -900)
            x3 = x2 + 900;
    }

}
