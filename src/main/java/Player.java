import com.brashmonkey.spriter.Entity;

import java.awt.event.KeyEvent;
import java.util.List;

public class Player extends com.brashmonkey.spriter.Player {

    public Player(Entity entity) {
        super(entity);
        setPosition(100, 425);
        setAnimation(0);
    }

    public void update(List<Integer> keyCodes) {
        super.update();

        if (keyCodes.contains(KeyEvent.VK_UP))
            setPosition(100, getY() + 5);
        else if (keyCodes.contains(KeyEvent.VK_DOWN))
            setPosition(100, getY() - 5);
    }

}
